#!/bin/bash
CLUSTER_NAME=$1
TIMEOUT=$2
INTERVAL=$3
END_TIME=$((SECONDS + TIMEOUT))

while [ $SECONDS -lt $END_TIME ]; do
  status=$(aws eks describe-cluster --name "$CLUSTER_NAME" --query "cluster.status" --output text)
  if [ "$status" == "ACTIVE" ]; then
    echo "EKS cluster is now active"
    exit 0
  fi
  echo "Waiting for EKS cluster to be active..."
  sleep "$INTERVAL"
done

echo "Timeout waiting for EKS cluster to become active"
exit 1
